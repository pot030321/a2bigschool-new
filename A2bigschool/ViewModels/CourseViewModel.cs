﻿using A2bigschool.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace A2bigschool.ViewModels
{
    public class CourseViewModel
    {
        [Required]
        public string Place { get; set; }
        [Required]
        [FutureDate]
        public string Date { get; set; }
        [Required]
        [ValidTime]
        public string Time { get; set; }
        [Required]     
        public byte Category { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public DateTime GetDateTime()
        {
            DateTime dateTime;
            string formattedDate = "";
            // Chuyển đổi từ "dd/MM/yyyy" sang DateTime
            if (DateTime.TryParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                // Chuyển đổi DateTime sang chuỗi "yyyy/MM/dd"
                formattedDate = dateTime.ToString("yyyy/MM/dd");

                // Giờ bạn có thể sử dụng chuỗi formattedDate để lưu vào cơ sở dữ liệu
            }
            else
            {
                throw new FormatException("Invalid date or time format.");
            }
            return DateTime.Parse(string.Format("{0} {1}", formattedDate, Time));
        }
    }
}
