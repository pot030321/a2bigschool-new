﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A2bigschool.Startup))]
namespace A2bigschool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
